using System.ComponentModel.DataAnnotations;
using Destructurama.Attributed;

namespace WebApplicationB.Model
{
    public class Echo
    {
        [Required] public int SleepTime { set; get; }

        [Required]
        [LogMasked(PreserveLength = false)]
        public string SecretMessage { set; get; }
    }
}