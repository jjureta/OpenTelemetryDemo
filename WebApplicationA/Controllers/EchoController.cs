﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using MathNet.Numerics.Distributions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplicationB.Model;

namespace WebApplicationA.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EchoController : ControllerBase
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ILogger<EchoController> _logger;

        public EchoController(ILogger<EchoController> logger, IHttpClientFactory clientFactory)
        {
            _logger = logger;
            _clientFactory = clientFactory;
        }

        [HttpGet]
        public async Task<IActionResult> Echo()
        {
            Activity.Current?.AddBaggage("Activity.Current.AddBaggage", "test");
            Activity.Current?.AddTag("Activity.Current.AddTag", "Webapplication A");

            _logger.LogInformation("WebapplicationA: Start echo ...");
            var result = await OnEcho();
            _logger.LogInformation("WebapplicationA: End echo");
            return Ok(result);
        }
        
        [HttpPost]
        public async Task<IActionResult> EchoPost([FromBody] Echo echo)
        {
            Activity.Current?.AddBaggage("Activity.Current.AddBaggage", "test");
            Activity.Current?.AddTag("Activity.Current.AddTag", "Webapplication A");

            _logger.LogInformation("WebapplicationA: Start echo {@echo} ...", echo);
            var result = await OnEchoPost(echo);
            _logger.LogInformation("WebapplicationA: End echo");
            return Ok(result);
        }

        [HttpGet]
        [Route("exception")]
        public async Task<IActionResult> EchoException()
        {
            _logger.LogInformation("WebapplicationA: Start echo exception ...");
            var result = await OnException();
            _logger.LogInformation("WebapplicationA: Send echo exception");
            return Ok(result);
        }

        private async Task<string> OnEcho()
        {
            _logger.LogInformation("WebapplicationA: Start calling service B ...");

            var sleep = GetRandomSleep();

            _logger.LogInformation("WebapplicationA: Sleep time: {SleepTime:000} [ms]", sleep);

            var query = HttpUtility.ParseQueryString(string.Empty);
            query["sleepTime"] = sleep.ToString();
            var queryString = query.ToString();

            var request = new HttpRequestMessage(HttpMethod.Get,
                "https://localhost:65011/Echo?" + queryString);

            request.Headers.Add("Accept", "application/vnd.github.v3+json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            _logger.LogInformation("WebapplicationA: service B status: {Status}", response.IsSuccessStatusCode);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsStringAsync();
            else
                throw new Exception();
        }
        
        private async Task<string> OnEchoPost(Echo echo)
        {
            _logger.LogInformation("WebapplicationA: Start calling service B ...");

            var sleep = GetRandomSleep(echo.SleepTime);

            _logger.LogInformation("WebapplicationA: Sleep time: {SleepTime:000} [ms]", sleep);

            var request = new HttpRequestMessage(HttpMethod.Post,
                "https://localhost:65011/Echo")
            {
                Content = new StringContent(JsonSerializer.Serialize(echo), Encoding.UTF8, "application/json")
            };
            
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            _logger.LogInformation("WebapplicationA: service B status: {Status}", response.IsSuccessStatusCode);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsStringAsync();
            else
                throw new Exception();
        }

        private async Task<string> OnException()
        {
            _logger.LogInformation("start calling service B echo exception ...");

            var request = new HttpRequestMessage(HttpMethod.Get,
                "https://localhost:65011/Echo/exception");

            request.Headers.Add("Accept", "application/vnd.github.v3+json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            _logger.LogInformation("service B status: " + response.IsSuccessStatusCode);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsStringAsync();
            else
                throw new Exception();
        }

        private int GetRandomSleep()
        {
            return GetRandomSleep(2500);
        }
        
        private int GetRandomSleep(int mean)
        {
            var normalDist = new Normal(mean, 200);
            var sleep = (int) normalDist.Sample();
            if (sleep < 0)
            {
                sleep = 100;
            }
            return sleep;
        }
    }
}