﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplicationB.Model;

namespace WebApplicationB.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EchoController : ControllerBase
    {
        private static readonly ConcurrentBag<byte[]> _cb = new ConcurrentBag<byte[]>();
        
        private readonly ILogger<EchoController> _logger;

        public EchoController(ILogger<EchoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult>  Echo([FromQuery(Name = "sleepTime")] int sleepTime)
        {
            Activity.Current?.AddTag("Activity.Current.AddTag", "Webapplication B"); 
            
            _logger.LogInformation("WebapplicationB: Start echo ...");
            _logger.LogInformation("WebapplicationB: Going to sleep for: {SleepTime:000} [ms]", sleepTime);
            await Task.Delay(sleepTime);
            _logger.LogInformation("WebapplicationB: End echo");

            return Ok();
        }
        
        [HttpPost]
        public async Task<IActionResult> EchoPost([FromBody] Echo echo)
        {
            Activity.Current?.AddTag("Activity.Current.AddTag", "Webapplication B"); 
            
            _logger.LogInformation("WebapplicationB: Start echo {@echo} ...", echo);
            _logger.LogInformation("WebapplicationB: Going to sleep for: {SleepTime:000} [ms]", echo.SleepTime);
            //Thread.Sleep(1000);
            await Task.Delay(echo.SleepTime);
            _logger.LogInformation("WebapplicationB: End echo");

            return Ok(echo);
        }

        [HttpGet]
        [Route("exception")]
        public void EchoException()
        {
            _logger.LogInformation("WebapplicationB: Something bad is about to happen ...");
            throw new Exception("Test Exception");
        }
        
        [HttpGet]
        [Route("memory")]
        public void EchoMemory([FromQuery(Name = "size")] int size)
        {
            _logger.LogInformation("WebapplicationB: allocate a memory ...");
            _cb.Add(new byte[size * 1024]);
        }
    }
}